# REPOSITORY MOVED

## This repository has moved to

https://github.com/ignitionrobotics/docs

## Issues and pull requests are backed up at

https://osrf-migration.github.io/ignition-gh-pages/#!/ignitionrobotics/docs

## From May 15th to May 31st 2020, this read-only mercurial repository will be at

https://bitbucket.org/osrf-migrated/docs

## More info at

https://community.gazebosim.org/t/important-gazebo-and-ignition-are-going-to-github/533

